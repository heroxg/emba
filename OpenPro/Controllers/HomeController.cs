﻿using OpenPro.Areas.Profile.Controllers;
using OpenPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenPro.Controllers
{
    //[Autheticate]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int id = Convert.ToInt32(Session["id"]);
            ProfileInfo profile = Util.GetProfile(id);

            //using (DBContext db = new DBContext())
            //{
            //    var proID = db.BasicInfos.Where(x => x.ID == id).First();
            //    var image = db.ProfileImages.Where(x => x.BasicInfoID.ID == id).FirstOrDefault();
            //    profile.BasicInfo.ProfilePicture = image == null ? "" : image.Image;
            //}

            Session["name"] = $"{profile.BasicInfo.LastName} {profile.BasicInfo.FirstName}";
            Session["image"] = $"{profile.BasicInfo.ProfilePicture}";
            ViewData["image"] = $"{profile.BasicInfo.ProfilePicture}";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Blog()
        {
            return View();
        }
        public ActionResult SearchResult()
        {
            return View();
        }

        public ActionResult ViewClassMate(int? id)
        {
            if (id != null && id != 0)
            {
                ProfileInfo profile = Util.GetProfile(id.Value);
                return View("Profile", profile);
            }
            else
            {
                var profiles = Util.GetProfiles();
                return View(profiles);
            }
        }

        [HttpPost]
        public ActionResult ViewClassMate(string searchTxt)
        {
            List<ProfileInfo> profiles = new List<ProfileInfo>();
            using (DBContext db = new DBContext())
            {
                var users = db.Database.SqlQuery<Entities.BasicInfo>("SELECT * FROM basicinfo WHERE MATCH(`LastName`, `FirstName`,`Designation`,`MiddleName`,`Email`)" +
                        "AGAINST({0} IN NATURAL LANGUAGE MODE) ", searchTxt).ToList();
                foreach (var user in users)
                {
                    profiles.Add(Util.CastToProfile(user));
                }
            }
            return View(profiles);
        }
    }
}