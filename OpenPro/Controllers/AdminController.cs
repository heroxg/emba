﻿using OpenPro.Entities;
using OpenPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenPro.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            List<UserModel> users = new List<UserModel>();
            try
            {
                using (DBContext db = new DBContext())
                {
                    users = db.BasicInfos.Select(x => new UserModel { Email = x.Email, ID = x.ID, LastName = x.LastName, FirstName = x.FirstName }).ToList();
                }
            }
            catch
            {
                RedirectToAction("Index", "Home");
            }
            return View(users);
        }

        // GET: Admin/Details/5
        public ActionResult Details(int id)
        {
            UserModel user = new UserModel();
            try
            {
                using (DBContext db = new DBContext())
                {
                    user = db.BasicInfos.Where(x => x.ID == id).Select(x => new UserModel { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName, ID = x.ID }).FirstOrDefault();
                }
            }
            catch
            {
                RedirectToAction("Index", "Admin");
            }

            return View(user);
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(UserModel login)
        {
            try
            {
                // TODO: Add insert logic here
                string password = Util.GetHashString(login.LastName.ToLower());
                using (DBContext db = new DBContext())
                {
                    db.Users.Add(new Entities.Users { UserName = login.Email, Password = password });
                    db.BasicInfos.Add(new Entities.BasicInfo { Email = login.Email, FirstName = login.FirstName, LastName = login.LastName });
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Exception error = ex;
                do
                {                    
                    ViewBag.Error = error.Message;
                    error = error.InnerException;
                }
                while (error != null);
                return View();
            }
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id)
        {
            using (DBContext db = new DBContext())
            {
                var user = db.BasicInfos.Where(x => x.ID == id).Select(x => new UserModel { ID = x.ID, Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).FirstOrDefault();
                return View(user);
            }
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, UserModel login)
        {
            try
            {
                // TODO: Add update logic here
                string password = Util.GetHashString(login.LastName.ToLower());
                using (DBContext db = new DBContext())
                {
                    var user = db.Users.Where(x => x.UserName == login.Email).First();
                    user.Password = password;

                    var basic = db.BasicInfos.Where(x => x.Email == login.Email).First();
                    basic.FirstName = login.FirstName;
                    basic.LastName = login.LastName;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id)
        {
            using (DBContext db = new DBContext())
            {
                var user = db.BasicInfos.Where(x => x.ID == id).Select(x => new UserModel { ID = x.ID, Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).FirstOrDefault();
                return View(user);
            }
        }

        // POST: Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Login login)
        {
            try
            {
                // TODO: Add delete logic here
                // var user = new Users { ID = id };
                using (DBContext db = new DBContext())
                {
                    var user = db.BasicInfos.Where(x => x.ID == id).FirstOrDefault();
                    db.Entry(user).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
