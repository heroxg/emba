﻿using OpenPro.Areas.Profile.Controllers;
using OpenPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenPro.Controllers
{
    //[Autheticate]
    public class AccountController : Controller
    {
        // GET: Account
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {
            var user = Util.Login(login);
            try
            {
                if (user != null)
                {
                    Session["id"] = user.ID;
                    Session["email"] = user.UserName;
                    return RedirectToAction("Index", "Home");
                }               
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
            ViewBag.Error = "Email or Password deos not match";
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}