﻿using System;
using System.Web.Mvc;

namespace OpenPro.Areas.Profile.Controllers
{
    internal class AutheticateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(filterContext.HttpContext.Session["email"] == null)
            {
                filterContext.Result = new RedirectResult($"~/Account/Login");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}