﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using OpenPro.Models;

namespace OpenPro.Areas.Profile.Controllers
{

    [Autheticate]
    public class DashboardController : Controller
    {
        // GET: Profile/Home
        [Route("Profile/Home")]
        public ActionResult Index()
        {
            int id = Convert.ToInt32(Session["id"]);
            ProfileInfo profile = Util.GetProfile(id);

            //using(DBContext db = new DBContext())
            //{
            //    var proID = db.BasicInfos.Where(x=> x.ID ==  id).First();
            //    var image = db.ProfileImages.Where(x => x.BasicInfoID.ID == id).FirstOrDefault();
            //    profile.BasicInfo.ProfilePicture = image == null ? "" : image.Image;
            //}

            Session["name"] = $"{profile.BasicInfo.LastName} {profile.BasicInfo.FirstName}";
            Session["image"] = $"{profile.BasicInfo.ProfilePicture}";
            ViewData["image"] = $"{profile.BasicInfo.ProfilePicture}";
            return View(profile);
        }

        public ActionResult Edit()
        {
            int id = Convert.ToInt32(Session["id"]);
            ProfileInfo profile = Util.GetProfile(id);
            return View(profile);
        }

        public ActionResult BasicInfo(BasicInfo form)
        {
            int id = Convert.ToInt32(Session["id"]);
            Entities.BasicInfo basic = new Entities.BasicInfo();
            if (string.IsNullOrWhiteSpace(form.Email))
            {
                ProfileInfo profile = Util.GetProfile(id);
                return View(profile);
            }
            else
            {
                try
                {
                    using (DBContext db = new DBContext())
                    {
                        basic = db.BasicInfos.Where(x => x.ID == id).First();
                        basic.LastName = form.LastName;
                        basic.FirstName = form.FirstName;
                        basic.MiddleName = form.MiddleName;
                        basic.DateOfBirth = string.IsNullOrWhiteSpace(form.DateOfBirth) ? DateTime.MinValue : Convert.ToDateTime(form.DateOfBirth);
                        basic.Designation = form.Designation;
                        basic.Email = form.Email;
                        basic.Facebook = form.Facebook;
                        basic.LinkedIn = form.LinkedIn;
                        basic.Phone = form.Phone;
                        //if (basic..ProfileImage == null) { basic.ProfileImage = new Entities.ProfileImage { BasicInfoID = basic }; }
                        //basic.ProfileImage.Image = form.ProfilePicture;
                        basic.Twitter = form.Twitter;
                        basic.WeddingAnniversary = string.IsNullOrWhiteSpace(form.WeddingAnniversary) ? DateTime.MinValue : Convert.ToDateTime(form.WeddingAnniversary);
                        
                        var image = db.ProfileImages.Where(x => x.BasicInfoID.ID == basic.ID).FirstOrDefault();
                        if (image == null)
                        {
                            db.ProfileImages.Add(new Entities.ProfileImage { Image = form.ProfilePicture, BasicInfoID = basic });
                        }
                        else
                        {
                            image.Image = form.ProfilePicture;
                        }

                        db.SaveChanges();
                    }
                    return RedirectToAction("Academics");
                }
                catch (Exception ex)
                {
                    ProfileInfo profile = Util.GetProfile(id);
                    profile.BasicInfo = new BasicInfo { Email = basic.Email, DateOfBirth = basic.DateOfBirth.ToShortDateString(), Designation = basic.Designation,
                                            Facebook = basic.Facebook, Twitter = basic.Twitter, FirstName = basic.FirstName, LastName = basic.LastName, LinkedIn = basic.LinkedIn,
                                            MiddleName = basic.MiddleName, Phone = basic.Phone, WeddingAnniversary = basic.WeddingAnniversary.ToShortDateString(),
                                            //ProfilePicture = basic.ProfileImage.Image
                                        };
                    Exception error = ex;
                    do
                    {
                        ViewBag.Error = error.Message;
                        error = ex.InnerException;
                    }
                    while (error.InnerException != null);
                    return View(profile);
                }
            }
        }

        public ActionResult Business()
        {
            int id = Convert.ToInt32(Session["id"]);
            ProfileInfo profile = Util.GetProfile(id);

            return View(profile);
        }

        [HttpPost]
        public string SavePic(FormCollection form)
        {
            int id = Convert.ToInt32(Session["id"]);
            using(DBContext db = new DBContext())
            {
                var basic = db.BasicInfos.Where(x => x.ID == id).First();
                var image = db.ProfileImages.Where(x => x.BasicInfoID.ID == basic.ID).FirstOrDefault();
                if (image == null)
                {
                    db.ProfileImages.Add(new Entities.ProfileImage { Image = form["profilepicture"], BasicInfoID = basic });
                }
                else
                {
                    image.Image = form["ProfilePicture"];
                }
                db.SaveChanges();                
            }
            return "saved";
        }

        [HttpPost]
        public ActionResult Business(BusinessInfo forms)
        {
            int id = Convert.ToInt32(Session["id"]);
            
            try
            {
                using (DBContext db = new DBContext())
                {
                    //foreach (var form in forms)
                    {
                        var f = db.BusinessInfos.Where(x => x.BasicInfoID.ID == id).FirstOrDefault();
                        if(f == null)
                        {
                            f = new Entities.BusinessInfo();
                            f.BasicInfoID = db.BasicInfos.Where(x => x.ID == id).First();
                            db.Entry(f).State = EntityState.Added;
                        }
                        f.BusinessDescription = forms.BusinessDescription;
                        f.BusinessName = forms.BusinessName;
                        f.BusinessType = forms.BusinessType;
                        f.Email = forms.Email;
                        f.Phone = forms.Phone;
                        f.Website = forms.Website;
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                ProfileInfo profile = Util.GetProfile(id);
                return View(profile);
            }
        }

        public ActionResult Academics()
        {
            int id = Convert.ToInt32(Session["id"]);

            ProfileInfo profile = Util.GetProfile(id);
            return View(profile);

        }

        [HttpPost]
        public ActionResult Academics(AcademicInfo forms)
        {
            int id = Convert.ToInt32(Session["id"]);
            //if (string.IsNullOrWhiteSpace(forms.Institute))
            //{
            //    ProfileInfo profile = Util.GetProfile(id);
            //    return View(profile);
            //}
            try
            {

                using (DBContext db = new DBContext())
                {
                    //foreach (var form in forms)
                    {
                        var a = db.AcademicInfos.Where(x => x.BasicInfoID.ID == id).FirstOrDefault();
                        if (a == null)
                        {
                            a = new Entities.AcademicInfo();
                            a.BasicInfoID = db.BasicInfos.Where(x => x.ID == id).First();
                            db.Entry(a).State = EntityState.Added;
                        }

                        a.Degree = forms.Degree;
                        a.EndDate = new DateTime(forms.To, 1, 1, 1, 1, 1);
                        a.Institution = forms.Institute;
                        a.StartDate = new DateTime(forms.From, 1, 1, 1, 1, 1);


                    }
                    db.SaveChanges();
                }
                return RedirectToAction("Biography");
            }
            catch (Exception ex)
            {
                return View(Profile);
            }
        }

        public ActionResult Biography()
        {
            int id = Convert.ToInt32(Session["id"]);
            ProfileInfo profile = Util.GetProfile(id);
            return View(profile.BasicInfo);
        }

        [HttpPost]
        public ActionResult Biography(BasicInfo form)
        {
            int id = Convert.ToInt32(Session["id"]);

            //if (string.IsNullOrWhiteSpace(form.Biography))
            //{
            //    ProfileInfo profile = Util.GetProfile(id);
            //    return View(profile.BasicInfo);
            //}
            //else
            {
                using (DBContext db = new DBContext())
                {
                    var basic = db.BasicInfos.Where(x => x.ID == id).First();
                    basic.Biography = form.Biography;
                    db.SaveChanges();
                }
                return RedirectToAction("Business");
            }
        }
    }
}