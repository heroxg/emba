﻿using EntityFramework.Functions;
using MySql.Data.MySqlClient;
using OpenPro.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure.Interception;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace OpenPro
{
    //[DbConfigurationType(typeof(MySqlConfiguration))]
    public class DBContext : DbContext
    {
        //static DBContext()
        //{
        //    DbInterception.Add(new FtsInterceptor());
        //}
        public DBContext() : base("ConString") { }

        public DbSet<Users> Users { get; set; }
        public DbSet<BasicInfo> BasicInfos { get; set; }
        public DbSet<SocialMedia> SocialMedias { get; set; }
        public DbSet<ProfileImage> ProfileImages { get; set; }
        public DbSet<AcademicInfo> AcademicInfos { get; set; }
        public DbSet<BusinessInfo> BusinessInfos { get; set; }
        //public DbSet<Note> Notes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Configurations.Add(new BasicInfoMap());
            //modelBuilder.Configurations.Add(new NoteMap());

            //modelBuilder.Conventions.Add(new FunctionConvention());
            //modelBuilder.AddComplexTypesFromAssembly(typeof(BasicInfoIds).Assembly);

            //DbInterception.Add(new SearchTermInterceptor());
        }

        //[Function(FunctionType.TableValuedFunction, nameof(fnSampleTableFTS), "dummyNamespace", Schema = "dbo")]
        //public IQueryable fnSampleTableFTS([Parameter(DbType = "nvarchar")]string searchterm)
        //{
        //    ObjectParameter searchtermParameter;
        //    if (string.IsNullOrWhiteSpace(searchterm))
        //        searchtermParameter = new ObjectParameter(SearchTermInterceptor.SEARCHTERMNAME, typeof(string));
        //    else
        //        searchtermParameter = new ObjectParameter(SearchTermInterceptor.SEARCHTERMNAME, String.Format("{0}", searchterm.AndPartsFormatter(new object())));

        //    return this.ObjectContext().CreateQuery<int>(
        //        $"[{nameof(this.fnSampleTableFTS)}](@{SearchTermInterceptor.SEARCHTERMNAME})", searchtermParameter);
        //}
    }

    //public class FtsInterceptor : IDbCommandInterceptor
    //{
    //    private const string FullTextPrefix = "-FTSPREFIX-";
    //    public static string Fts(string search)
    //    {
    //        return string.Format("({0}{1})", FullTextPrefix, search);
    //    }
    //    public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
    //    {
    //    }
    //    public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
    //    {
    //    }
    //    public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
    //    {
    //        RewriteFullTextQuery(command);
    //    }
    //    public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
    //    {
    //    }
    //    public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
    //    {
    //        RewriteFullTextQuery(command);
    //    }
    //    public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
    //    {
    //    }
    //    public static void RewriteFullTextQuery(DbCommand cmd)
    //    {
    //        string text = cmd.CommandText;
    //        for (int i = 0; i < cmd.Parameters.Count; i++)
    //        {
    //            DbParameter parameter = cmd.Parameters[i];
    //            if (parameter.DbType.In(DbType.String, DbType.AnsiString, DbType.StringFixedLength, DbType.AnsiStringFixedLength))
    //            {
    //                if (parameter.Value == DBNull.Value)
    //                    continue;
    //                var value = (string)parameter.Value;
    //                if (value.IndexOf(FullTextPrefix) >= 0)
    //                {
    //                    parameter.Size = 4096;
    //                    parameter.DbType = DbType.AnsiStringFixedLength;
    //                    value = value.Replace(FullTextPrefix, ""); // remove prefix we added n linq query
    //                    value = value.Substring(1, value.Length - 2); // remove %% escaping by linq translator from string.Contains to sql LIKE
    //                    parameter.Value = value;
    //                    cmd.CommandText = Regex.Replace(text,
    //                    string.Format(
    //                    @"\[(\w*)\].\[(\w*)\]\s*LIKE\s*@{0}\s?(?:ESCAPE 
    //                    N?'~')", parameter.ParameterName),
    //                    string.Format(@"contains([$1].[$2], @{0})", parameter.ParameterName));
    //                    if (text == cmd.CommandText)
    //                        throw new Exception("FTS was not replaced on: " + text);
    //                    text = cmd.CommandText;
    //                }
    //            }
    //        }
    //    }
    //}

    //static class LanguageExtensions
    //{
    //    public static bool In<T>(this T source, params T[] list)
    //    {
    //        return (list as IList<T>).Contains(source);
    //    }
    //}

    ////public class BasicInfoMap : EntityTypeConfiguration<BasicInfo>
    ////{
    ////    public BasicInfoMap()
    ////    {
    ////        // Primary Key
    ////        HasKey(t => t.ID);
    ////    }
    ////}

    //public class Note
    //{
    //    public int Id { get; set; }
    //    public string NoteText { get; set; }
    //}

    //public class NoteMap : EntityTypeConfiguration<Note>
    //{
    //    public NoteMap()
    //    {
    //        // Primary Key
    //        HasKey(t => t.Id);
    //    }
    //}
    ////public static class SearchTermFormatExtentions
    //{
    //    private static string SanitizeSearchTerm(string searchPhrase)
    //    {
    //        if (searchPhrase.Length > 200)
    //        {
    //            searchPhrase = searchPhrase.Substring(0, 200);
    //        }

    //        searchPhrase = searchPhrase.Replace(";", " ");
    //        searchPhrase = searchPhrase.Replace("'", " ");
    //        searchPhrase = searchPhrase.Replace("--", " ");
    //        searchPhrase = searchPhrase.Replace("/*", " ");
    //        searchPhrase = searchPhrase.Replace("*/", " ");
    //        searchPhrase = searchPhrase.Replace("xp_", " ");
    //        return searchPhrase;
    //    }

    //    public static string AndPartsFormatter(this string searchterm, object t)
    //    {
    //        var searchterms = SanitizeSearchTerm(searchterm).Split(' ');
    //        searchterms = searchterms.Select(s => "\"" + new string(s.Where(c => char.IsLetterOrDigit(c)).ToArray()) + "*\"").ToArray();
    //        searchterm = string.Join(" AND ", searchterms);
    //        return searchterm;
    //    }
    //}

    //public class SearchTermInterceptor : IDbCommandInterceptor
    //{
    //    public const string SEARCHTERMNAME = "fulltextsearchparam";

    //    public static void RewriteSearchTerm(DbCommand cmd)
    //    {
    //        string text = cmd.CommandText;
    //        for (int i = 0; i < cmd.Parameters.Count; i++)
    //        {
    //            DbParameter parameter = cmd.Parameters[i];
    //            if (parameter.ParameterName == SEARCHTERMNAME)
    //            {
    //                parameter.Size = 1000;
    //            }
    //        }
    //    }

    //    public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
    //    {
    //        //throw new NotImplementedException();
    //    }

    //    public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
    //    {
    //        //throw new NotImplementedException();
    //    }

    //    public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
    //    {
    //        //throw new NotImplementedException();
    //    }

    //    public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
    //    {
    //        //throw new NotImplementedException();
    //    }

    //    public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
    //    {
    //        //throw new NotImplementedException();
    //    }

    //    public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
    //    {
    //        //throw new NotImplementedException();
    //    }
    //}


}