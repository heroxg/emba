namespace OpenPro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.academicinfo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Institution = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Degree = c.String(maxLength: 50, storeType: "nvarchar"),
                        StartDate = c.DateTime(nullable: false, precision: 0),
                        EndDate = c.DateTime(nullable: false, precision: 0),
                        BasicInfoID_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.basicinfo", t => t.BasicInfoID_ID, cascadeDelete: true)
                .Index(t => t.BasicInfoID_ID);
            
            CreateTable(
                "dbo.basicinfo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Designation = c.String(maxLength: 100, storeType: "nvarchar"),
                        LastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        FirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        MiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        Email = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                        Phone = c.String(maxLength: 100, storeType: "nvarchar"),
                        DateOfBirth = c.DateTime(nullable: false, precision: 0),
                        WeddingAnniversary = c.DateTime(nullable: false, precision: 0),
                        Biography = c.String(maxLength: 600, storeType: "nvarchar"),
                        Facebook = c.String(maxLength: 200, storeType: "nvarchar"),
                        LinkedIn = c.String(maxLength: 200, storeType: "nvarchar"),
                        Twitter = c.String(maxLength: 200, storeType: "nvarchar"),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.users", t => t.User_ID)
                .Index(t => t.LastName, clustered: true)
                .Index(t => t.FirstName, clustered: true)
                .Index(t => t.Email, unique: true)
                .Index(t => t.Phone)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.businessinfo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BusinessName = c.String(maxLength: 100, storeType: "nvarchar"),
                        BusinessType = c.String(maxLength: 100, storeType: "nvarchar"),
                        BusinessDescription = c.String(maxLength: 1000, storeType: "nvarchar"),
                        Website = c.String(maxLength: 100, storeType: "nvarchar"),
                        Email = c.String(maxLength: 100, storeType: "nvarchar"),
                        Phone = c.String(maxLength: 50, storeType: "nvarchar"),
                        BasicInfoID_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.basicinfo", t => t.BasicInfoID_ID, cascadeDelete: true)
                .Index(t => t.BusinessType)
                .Index(t => t.BasicInfoID_ID);
            
            CreateTable(
                "dbo.users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        username = c.String(maxLength: 100, storeType: "nvarchar"),
                        password = c.String(maxLength: 200, storeType: "nvarchar"),
                        roles = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.username, unique: true);
            
            CreateTable(
                "dbo.profileimage",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Image = c.String(unicode: false),
                        BasicInfoID_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.basicinfo", t => t.BasicInfoID_ID, cascadeDelete: true)
                .Index(t => t.BasicInfoID_ID);
            
            CreateTable(
                "dbo.socialmedia",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Url = c.String(maxLength: 250, storeType: "nvarchar"),
                        BasicInfoID_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.basicinfo", t => t.BasicInfoID_ID, cascadeDelete: true)
                .Index(t => t.BasicInfoID_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.socialmedia", "BasicInfoID_ID", "dbo.basicinfo");
            DropForeignKey("dbo.profileimage", "BasicInfoID_ID", "dbo.basicinfo");
            DropForeignKey("dbo.academicinfo", "BasicInfoID_ID", "dbo.basicinfo");
            DropForeignKey("dbo.basicinfo", "User_ID", "dbo.users");
            DropForeignKey("dbo.businessinfo", "BasicInfoID_ID", "dbo.basicinfo");
            DropIndex("dbo.socialmedia", new[] { "BasicInfoID_ID" });
            DropIndex("dbo.profileimage", new[] { "BasicInfoID_ID" });
            DropIndex("dbo.users", new[] { "username" });
            DropIndex("dbo.businessinfo", new[] { "BasicInfoID_ID" });
            DropIndex("dbo.businessinfo", new[] { "BusinessType" });
            DropIndex("dbo.basicinfo", new[] { "User_ID" });
            DropIndex("dbo.basicinfo", new[] { "Phone" });
            DropIndex("dbo.basicinfo", new[] { "Email" });
            DropIndex("dbo.basicinfo", new[] { "FirstName" });
            DropIndex("dbo.basicinfo", new[] { "LastName" });
            DropIndex("dbo.academicinfo", new[] { "BasicInfoID_ID" });
            DropTable("dbo.socialmedia");
            DropTable("dbo.profileimage");
            DropTable("dbo.users");
            DropTable("dbo.businessinfo");
            DropTable("dbo.basicinfo");
            DropTable("dbo.academicinfo");
        }
    }
}
