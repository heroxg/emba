namespace OpenPro.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class current : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Note");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Note",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NoteText = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
