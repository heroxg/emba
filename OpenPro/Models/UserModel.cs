﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenPro.Models
{
    public class UserModel
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public int ID { get; set; }
    }
}