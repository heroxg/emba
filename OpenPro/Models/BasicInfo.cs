﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OpenPro.Models
{
    public class BasicInfo
    {
        public int ID { get; set; }

        [DataType(DataType.Text)]
        public string Designation { get; set; }
        
        [DataType(DataType.Text)]
        public string LastName { get; set; }

        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        public string MiddleName { get; set; }
                
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [DataType(DataType.Date)]
        public string DateOfBirth { get; set; }

        [DataType(DataType.Date)]
        public string WeddingAnniversary { get; set; }

        [DataType(DataType.MultilineText)]
        public string Biography { get; set; }

        [DataType(DataType.Url)]
        public string Facebook { get; set; }

        [DataType(DataType.Url)]
        public string LinkedIn { get; set; }

        [DataType(DataType.Url)]
        public string Twitter { get; set; }

        public string ProfilePicture { get; set; }
    }
}