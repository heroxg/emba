﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenPro.Models
{
    public class ProfileInfo
    {
        public BasicInfo BasicInfo { get; set; }
        public List<AcademicInfo> Academics { get; set; }
        public List<BusinessInfo> Businesses { get; set; }
    }
}