﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenPro.Models
{
    public class AcademicInfo
    {
        public string Institute { get; set; }
        public string Degree { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public int ID { get; set; }
    }
}