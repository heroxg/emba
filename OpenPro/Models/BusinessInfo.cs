﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OpenPro.Models
{
    public class BusinessInfo
    {
        [DataType(DataType.Text)]
        public string BusinessName { get; set; }

        [DataType(DataType.Text)]
        public string BusinessType { get; set; }

        [DataType(DataType.MultilineText)]
        public string BusinessDescription { get; set; }

        [DataType(DataType.Url)]
        public string Website { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public int ID { get; set; }
    }
}