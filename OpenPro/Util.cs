﻿using OpenPro.Models;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace OpenPro
{
    public static class Util
    {
        internal static Entities.Users Login(Login login)
        {
            var password = GetHashString(login.Password);
            using (DBContext db = new DBContext())
            {
                var user = db.Users
                    .Where(x => x.UserName == login.Email && x.Password == password).FirstOrDefault();
                return user;
            }
        }

        public static ProfileInfo GetProfile(int id)
        {
            try
            {
                using (DBContext db = new DBContext())
                {
                    var user = db.BasicInfos
                        .Include(x => x.Academics)
                        .Include(x => x.Businesses)
                        //.Include(x => x.ProfileImage)
                        .Where(x => x.ID == id).FirstOrDefault();
                    return Util.CastToProfile(user);
                }
            }
            catch (Exception ex)
            {
                return new ProfileInfo { };
            }

        }

        public static List<ProfileInfo> GetProfiles()
        {
            var profiles = new List<ProfileInfo>();
            try
            {
                var Users = new List<Entities.BasicInfo>();
                using (DBContext db = new DBContext())
                {
                    var users = db.BasicInfos
                        .Include(x => x.Academics)
                        .Include(x => x.Businesses)
                        //.Include(x => x.ProfileImage)
                        .ToList();
                    Users = users;
                }
                foreach (var user in Users)
                {
                    profiles.Add(CastToProfile(user));
                }
                return profiles;
            }
            catch (Exception ex)
            {
                return new List<ProfileInfo> { };
            }
        }

        public static ProfileInfo CastToProfile(Entities.BasicInfo profile)
        {
            ProfileInfo pro = new ProfileInfo();
            pro.BasicInfo = new BasicInfo();
            pro.Academics = new List<AcademicInfo>();
            pro.Businesses = new List<BusinessInfo>();

            pro.BasicInfo.ID = profile.ID;
            pro.BasicInfo.Biography = profile.Biography ?? "";
            pro.BasicInfo.DateOfBirth = profile.DateOfBirth != DateTime.MinValue ? profile.DateOfBirth.ToShortDateString() : "";
            pro.BasicInfo.Designation = profile.Designation ?? "";
            pro.BasicInfo.Email = profile.Email ?? "";
            pro.BasicInfo.Facebook = profile.Facebook ?? "";
            pro.BasicInfo.FirstName = profile.FirstName ?? "";
            pro.BasicInfo.LastName = profile.LastName ?? "";
            pro.BasicInfo.LinkedIn = profile.LinkedIn ?? "";
            pro.BasicInfo.MiddleName = profile.MiddleName ?? "";
            pro.BasicInfo.Phone = profile.Phone ?? "";
            //pro.BasicInfo.ProfilePicture = profile.ProfileImage != null && !string.IsNullOrWhiteSpace(profile.ProfileImage.Image) ? profile.ProfileImage.Image : "";
            pro.BasicInfo.Twitter = profile.Twitter ?? "";
            pro.BasicInfo.WeddingAnniversary = profile.WeddingAnniversary != DateTime.MinValue ? profile.WeddingAnniversary.ToShortDateString() : "";

            using (DBContext db = new DBContext())
            {
                var image = db.ProfileImages.Where(x => x.BasicInfoID.ID == profile.ID).FirstOrDefault();
                pro.BasicInfo.ProfilePicture = image == null ? "" : image.Image;
            }

            if (profile.Academics != null)
            {
                foreach (var aca in profile.Academics)
                {
                    pro.Academics.Add(new AcademicInfo
                    {
                        ID = aca.ID,
                        Degree = aca.Degree,
                        From = aca.StartDate.Year,
                        Institute = aca.Institution,
                        To = aca.EndDate.Year
                    });
                }
            }

            if(profile.Businesses != null)
            {
                foreach (var biz in profile.Businesses)
                {
                    pro.Businesses.Add(new BusinessInfo
                    {
                        BusinessDescription = biz.BusinessDescription,
                        BusinessName = biz.BusinessName,
                        BusinessType = biz.BusinessType,
                        Email = biz.Email,
                        Phone = biz.Phone,
                        Website = biz.Website,
                        ID = biz.ID
                    });
                }
            }            

            return pro;
        }


        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}