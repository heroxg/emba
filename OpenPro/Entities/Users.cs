﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OpenPro.Entities
{
    [Table("users")]
    public class Users
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(100)]
        [Column("username")]
        [Index(IsUnique = true)]
        public string UserName { get; set; }

        [MaxLength(200)]
        [Column("password")]
        public string Password { get; set; }

        [Column("roles")]
        public UserRoles Roles { get; set; }
    }

    public enum UserRoles
    {
        Admin,
        User
    }
}