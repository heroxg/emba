﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OpenPro.Entities
{
    [Table("profileimage")]
    public class ProfileImage
    {
        [Key]
        //[Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
               
        //[Column("image")]
        public string Image { get; set; }

        [Key]
        [Required]
        public BasicInfo BasicInfoID { get; set; }
    }
}