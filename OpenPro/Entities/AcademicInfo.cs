﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OpenPro.Entities
{
    [Table("academicinfo")]
    public class AcademicInfo
    {
        [Key]
        //[Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Required]
        //[Column("basicinfoid")]
        public virtual BasicInfo BasicInfoID { get; set; }

        [Required]
        [MaxLength(100)]
        //[Column("institution")]
        public string Institution { get; set; }

        [MaxLength(50)]
        //[Column("degree")]
        public string Degree { get; set; }

        //[Column("startdate")]
        public DateTime StartDate { get; set; }

        //[Column("enddate")]
        public DateTime EndDate { get; set; }
    }
}