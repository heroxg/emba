﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OpenPro.Entities
{
    [Table("basicinfo")]
    public class BasicInfo
    {
        [Key]
        //[Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(100)]
        //[Column("designation")]
        public string Designation { get; set; }

        [MaxLength(100)]
        //[Column("lastname")]
        [Index(IsClustered = true, Order = 1)]
        public string LastName { get; set; }

        [MaxLength(100)]
        //[Column("firstname")]
        [Index(IsClustered = true, Order = 2)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        //[Column("middlename")]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(100)]
        //[Column("email")]
        [Index(IsUnique = true)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Index]
        [MaxLength(100)]
        //[Column("phone")]
        public string Phone { get; set; }

        //[Column("dateofbirth")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [DataType(DataType.Date)]
        //[Column("weddinganniversary")]
        public DateTime WeddingAnniversary { get; set; }

        [MaxLength(600)]
        //[Column("biography")]
        [DataType(DataType.Text)]
        public string Biography { get; set; }

        [MaxLength(200)]
        //[Column("facebook")]
        //[DataType(DataType.Url)]
        public string Facebook { get; set; }

        [MaxLength(200)]
        //[Column("linkedin")]
        //[DataType(DataType.Url)]
        public string LinkedIn { get; set; }

        [MaxLength(200)]
        //[Column("twitter")]
        //[DataType(DataType.Url)]
        public string Twitter { get; set; }

        //[Required]
        //[MaxLength(200)]
        //[DataType(DataType.Password)]
        //public string Password { get; set; }

        //[Column("academics")]
        public virtual List<AcademicInfo> Academics { get; set; }
        //[Column("businesses")]
        public virtual List<BusinessInfo> Businesses { get; set; }
        //[Column("profileimage")]
        //public ProfileImage ProfileImage { get; set; }

        [Key]
        //[Column("user")]
        public virtual Users User { get; set; }
    }
}