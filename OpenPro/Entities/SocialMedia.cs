﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OpenPro.Entities
{
    [Table("socialmedia")]
    public class SocialMedia
    {
        [Key]
        //[Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Required]
        //[Column("basicinfoid")]
        public virtual BasicInfo BasicInfoID { get; set; }

        //[Column("url")]
        [MaxLength(250)]
        public string Url { get; set; }
    }
}