﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OpenPro.Entities
{
    [Table("businessinfo")]
    public class BusinessInfo
    {
        [Key]
        //[Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Required]
        //[Column("basicinfoid")]
        public virtual BasicInfo BasicInfoID { get; set; }

        [MaxLength(100)]
        //[Column("businessname")]
        public string BusinessName { get; set; }

        [Index]
        [MaxLength(100)]
        //[Column("businesstype")]
        public string BusinessType { get; set; }

        [MaxLength(1000)]
        //[Column("businessdescription")]
        public string BusinessDescription { get; set; }

        [MaxLength(100)]
        //[Column("website")]
        public string Website { get; set; }

        [MaxLength(100)]
        //[Column("email")]
        public string Email { get; set; }

        [MaxLength(50)]
        //[Column("phone")]
        public string Phone { get; set; }

    }
}